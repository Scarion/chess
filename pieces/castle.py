from .piece import Piece, PieceType

class Castle(Piece):

    name = "C"
    pieceType = PieceType(4)