from enum import Enum

# Enum to determine the type of piece via a number
class PieceType(Enum):
    pawn = 1
    knight = 2
    bishop = 3
    castle = 4
    queen = 5
    king = 6

class PieceColour(Enum):
    white = 1
    black = 2

# Contains the standard behaviour of a piece
class Piece:

    colour = PieceColour(1)
    name = "-"
    pieceType = PieceType
    isSelected = False
    xCoord = 0
    yCoord = 0
    availableMoves = []

# Create a constructor for a piece
    def __init__(self, colour = PieceColour.white, xCoord = 0, yCoord = 0):

        self.colour = colour
        self.xCoord = xCoord
        self.yCoord = yCoord

# Print out the moves available for that particular piece 
    def printAvailableMoves(self):
        print("Available moves are: ")
        for move in range(len(self.availableMoves)):
            print(self.availableMoves[move])