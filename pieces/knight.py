from .piece import Piece, PieceType, PieceColour

class Knight(Piece):

    name = "N"
    pieceType = PieceType(2)

    def __init__(self, colour = PieceColour.white, xCoord = 0, yCoord = 0):
        super(Knight, self).__init__(...)
        self.availableMoves = [
            [self.xCoord + 1, self.yCoord + 2],
            [self.xCoord - 1, self.yCoord + 2],
            [self.xCoord + 1, self.yCoord - 2],
            [self.xCoord - 1, self.yCoord - 2],
            [self.xCoord + 2, self.yCoord + 1],
            [self.xCoord + 2, self.yCoord - 1],
            [self.xCoord - 2, self.yCoord + 1],
            [self.xCoord - 2, self.yCoord - 1],
        ]