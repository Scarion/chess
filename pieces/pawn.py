from .piece import Piece, PieceType, PieceColour

class Pawn(Piece):

    name = "P"
    pieceType = PieceType(1)

    def __init__(self, colour = PieceColour.white, xCoord = 0, yCoord = 0):
        super(Pawn, self).__init__(...)
        # Determine the direction based on the colour
        if self.colour == PieceColour.white:
            self.availableMoves = [[self.xCoord, self.yCoord + 1]]
        elif self.colour == PieceColour.black:
            self.availableMoves = [[self.xCoord, self.yCoord - 1]]