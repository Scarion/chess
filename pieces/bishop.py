from .piece import Piece, PieceType

class Bishop(Piece):

    name = "B"
    pieceType = PieceType(3)