from .piece import Piece, PieceType

class Queen(Piece):

    name = "Q"
    pieceType = PieceType(5)