from .piece import Piece, PieceType, PieceColour

class King(Piece):

    name = "K"
    pieceType = PieceType(6)
    
    def __init__(self, colour = PieceColour.white, xCoord = 0, yCoord = 0):
        super(King, self).__init__(...)
        self.availableMoves = [[self.xCoord + 1, self.yCoord], 
        [self.xCoord - 1, self.yCoord],
        [self.xCoord, self.yCoord + 1],
        [self.xCoord, self.yCoord - 1]]