from pieces import Piece, Pawn, Knight, Castle, Bishop, Queen, King, PieceColour
from boardSpace import BoardSpace
import pprint


class Board:
    board = []

    def __init__(self):
        for row in range(0, 8):
            self.board.append([])
            for column in range (0, 8):
                self.board[row].append(BoardSpace())

# Initialise the board to start the game
    def setupBoard(self):

        colour = PieceColour.white
         
        for row in range(0, len(self.board)):
            for column in range(0, len(self.board[row])):

                # Set the piece colour
                if row == 0 or row == 1:
                    colour = PieceColour.black
                else:
                    colour = PieceColour.white

                # If in the pawn row
                if row == 1:
                    self.board[row][column].occupyingPiece = Pawn(colour, row, column)
                if row == 6:
                    self.board[row][column].occupyingPiece = Pawn(colour, row, column)

                if row == 0 or row == 7:
                    # Castles
                    if column == 0 or column == 7:
                        self.board[row][column].occupyingPiece = Castle(colour, row, column)
                        
                    # Knights
                    elif column == 1 or column == 6:
                        self.board[row][column].occupyingPiece = Knight(colour, row, column)
                    
                    # Bishops
                    elif column == 2 or column == 5:
                        self.board[row][column].occupyingPiece = Bishop(colour, row, column)

                    # Queen
                    elif column == 3:
                        self.board[row][column].occupyingPiece = Queen(colour, row, column)

                    # King
                    elif column == 4:
                        self.board[row][column].occupyingPiece = King(colour, row, column)
        return

    # Ensures only one piece is selected at a time
    def selectPiece(self, xCoordinate = 0, yCoordinate = 0):
        selectSuccess = False

        # Cycle through all the pieces and set all but the chosen one selected
        for row in range(0, len(self.board)):
            for column in range(0, len(self.board[row])):
                # Check there is a piece in this place in the board
                if not self.board[row][column].occupyingPiece == None:
                    if column == xCoordinate and row == yCoordinate:
                        self.board[row][column].occupyingPiece.isSelected = True
                        selectSuccess = True
                    else:
                        # Set all other pieces to unselected
                        self.board[row][column].occupyingPiece.isSelected = False
        
        if selectSuccess == False:
            print("There is no piece here")
                
        
        return selectSuccess