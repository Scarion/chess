from pieces import Piece

class BoardSpace:

    occupied = False

    xCoordinate = 0
    yCoordinate = 0

    occupyingPiece = None

    def __str__(self):
        if self.occupyingPiece is None:
            return "None"
        else:
            return self.occupyingPiece.name
        # return "None" if self.occupyingPiece is None else self.occupyingPiece.name

    def __repr__(self):
        return self.__str__()