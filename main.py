import boardManager
from pieces import PieceType, Piece, PieceColour, Knight

# Allows a player to make a turn
def turn(gameBoard):
    validEntry = False
    columnCoordList = ["A", "B", "C", "D", "E", "F", "G", "H"]
    positionY = -1
    positionXStr = "I"
    positionX = -1

    # Check for a valid entry
    while not validEntry:
        try:
            selectedPosition = input('Your move: ').upper()
            # Check the entered command
            entryList = list(selectedPosition)
            if len(entryList) >= 2:
                positionXStr = list(selectedPosition)[0]
                positionY = int(list(selectedPosition)[1]) - 1

            # If the column coordinate was a valid letter
            if columnCoordList.count(positionXStr) == 1:
                positionX = columnCoordList.index(positionXStr)
                # If the y coordinate is within range
                if positionY >= 0 and positionY <= 7:
                    validEntry = True
            
            if not validEntry:
                print("Not a valid entry")

        except ValueError:
            print("That wasn't a number!")

    # Select the piece
    pieceSelected = gameBoard.selectPiece(positionX, positionY)
    if pieceSelected == True:
        # Hold a reference to the selected piece
        selectedPiece = gameBoard.board[positionY][positionX].occupyingPiece
        print("The selected piece is: ", selectedPiece.name, " ", selectedPiece.colour.name)

        # Get available moves for selected piece
        print(selectedPiece.printAvailableMoves())

    return

# Change the player's turn
def switchPlayer(playerSwitch = False):
    if playerSwitch == True:
        playerSwitch = False
    else:
        playerSwitch = True
    return playerSwitch

# Get the player name
def getPlayerName(playerSwitch = False):
    playerName = ""
    if playerSwitch == True:
        playerName = "Player 1"
    else:
        playerName = "Player 2"

    return playerName

# Function to print out the board
def displayBoard(gameBoard):
    for letter in ["  ", "A", "B", "C", "D", "E", "F", "G", "H"]:
            print(letter, end= '  ')
    print("\n")

    for row in range(0, len(gameBoard)):
        print(row + 1, end = ':  ')
        for column in range(0, len(gameBoard[row])):
            if not gameBoard[row][column].occupyingPiece == None:
                print(gameBoard[row][column].occupyingPiece.name, end='  ')
            else:
                print("-", end='  ')
        print("\n")
    return

playerSwitch = False
player = PieceColour(1)
gameOver = False
gameBoardManager = boardManager.Board()

gameBoardManager.setupBoard()

# Main loop of the game
while gameOver == False:

    # Display the game board
    displayBoard(gameBoardManager.board)
    
    # The player's turn
    turn(gameBoardManager)

    # Display the game board
    # displayBoard(gameBoardManager.board)

    # Switch the player
    switchPlayer(playerSwitch)
    
    gameOver = True


print("GAME OVER - ", getPlayerName(playerSwitch), " Wins!")